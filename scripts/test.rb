#!/usr/bin/env ruby

require 'json'
require 'deep_merge'
require 'pp'
require 'fileutils'

DEBUG=true

def get_file_list(dir)
  list = Dir.glob("#{dir}/*.json").select{ |f| File.file? f}
  puts list if DEBUG
  return list
end

def build_graph_data(list)
  @data = Hash.new
  list.each do |j|
    d = JSON.parse(File.read(j))
    tmp = Hash.new
    k = d['start']['system_info'].split(" ").values_at(1).join().to_s
    puts k if DEBUG
    tmp[k] = Hash.new
    e = d['start']['connecting_to']['host']
    tmp[k][e] = Hash.new
    tmp[k][e].merge!({ "tx" => "#{d['end']['sum_sent']['bits_per_second']}" })
    tmp[k][e].merge!({ "rx" => "#{d['end']['sum_received']['bits_per_second']}" })
    puts tmp if DEBUG
    @data.deep_merge(tmp)
  end
end

def build_graph()
  # Build graph vars
  keys = @data.keys.count
  hosts = @data[@data.keys.first].keys.count
  puts hosts if DEBUG
  x_axis_array = Array.new
  @data.keys.sort.each_with_index do |(k),index|
    t = [ "\"#{k}\" " "#{index.to_i}50," ]
    x_axis_array.push(t)
  end
  puts x_axis_array if DEBUG
  x_axis = x_axis_array.join(" ")
  bar_size = (100 / (hosts.to_i * 2 )).floor.to_i
  puts bar_size if DEBUG

  # Write out graphing dat file
  dat = '/tmp/iperf.dat'
  FileUtils.touch(dat)
  File.truncate(dat, 0)
  x = 0
  x2 = 0
  @data.keys.sort.each do |k|
    @data[k].keys.sort.each do |s|
      tx_w = "#{x + x2} #{(@data[k][s]['tx'].to_f / 1000000000).round(2)} #{s}"
      puts tx_w if DEBUG
      File.write(dat, "#{tx_w}\n", mode: "a")
      x2 += bar_size
      avg = ((@data[k][s]['tx'].to_f / 1000000000 / 8).round(2) + (@data[k][s]['tx'].to_f / 1000000000).round(2) / 2).round(2)
      rx_w = "#{x + x2} #{(@data[k][s]['tx'].to_f / 1000000000).round(2)} #{s} #{avg}"
      puts rx_w if DEBUG
      File.write(dat, "#{rx_w}\n", mode: "a")
      x2 += bar_size
    end
    x += 100
    x2 = 0
  end

  # Generate Graph
  gnuplot_args = [
    "set style line 1 lc rgb 'red'",
    "set style line 2 lc rgb 'blue'",
    "set title 'Iperf Throughput (Gbps)'",
    "set key on",
    "set ylabel 'Gbps'",
    "set xlabel 'Source'",
    "set boxwidth #{bar_size}",
    "set style fill solid",
    "set xtics (#{x_axis})",
    "plot '#{dat}' every 2 using 1:($2 + 1):3 with labels notitle rotate right, '#{dat}' every 2::1 using 1:($2 + 1):4 with labels notitle rotate right, '#{dat}' every 2 using 1:2 title 'TX' with boxes ls 1, '#{dat}' every 2::1 using 1:2 title 'RX' with boxes ls 2",
    "pause -1"
    
  ]
  puts gnuplot_args if DEBUG

  puts "gnuplot -e #{gnuplot_args.join("; ")}"
  system("gnuplot", "-e", "#{gnuplot_args.join("; ")}")

end

build_graph_data(get_file_list('/tmp/iperf_results'))

pp @data if DEBUG

build_graph()
