#!/bin/bash

{% for item in public_ips %}
/usr/bin/iperf3 -c {{ item }} --json > /root/tests/iperf/{{ inventory_hostname }}-to-{{ item }}.json
{% endfor %}
